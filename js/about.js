(function($) {
	
	var aboutJS = {

		loadWaypoints: function() {
			$('.go-down').waypoint(function(direction) {
				if ( direction == 'down' ) {
					$('.nav-container').addClass('fixed-nav');

					$('.list-dates.right-column').addClass('date1');
					$('.right-list li:nth-child(1)').addClass('on');
					$('.right-list li:nth-child(1) div').removeClass('not_visible');

					setTimeout(function() {
						$('.left-list li:nth-child(1)').addClass('on');
						$('.left-list li:nth-child(1) div').removeClass('not_visible');
					},500);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date1').addClass('date2');
					},850);		
					setTimeout(function() {
						$('.right-list li:nth-child(2)').addClass('on');
						$('.right-list li:nth-child(2) div').removeClass('not_visible');
					},1480);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date2').addClass('date3');
					},1850);
					setTimeout(function() {
						$('.left-list li:nth-child(2)').addClass('on');
						$('.left-list li:nth-child(2) div').removeClass('not_visible');
					},2480);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date3').addClass('date4');
					},2850);
					setTimeout(function() {
						$('.right-list li:nth-child(3)').addClass('on');
						$('.right-list li:nth-child(3) div').removeClass('not_visible');
					},3480);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date4').addClass('date5');
					},3850);
					setTimeout(function() {
						$('.left-list li:nth-child(3)').addClass('on');
						$('.left-list li:nth-child(3) div').removeClass('not_visible');
					},4480);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date5').addClass('date6');
					},4850);
					setTimeout(function() {
						$('.right-list li:nth-child(4)').addClass('on');
						$('.right-list li:nth-child(4) div').removeClass('not_visible');
					},5480);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date6').addClass('date7');
					},5850);
					setTimeout(function() {
						$('.left-list li:nth-child(4)').addClass('on');
						$('.left-list li:nth-child(4) div').removeClass('not_visible');
					},6250);
					setTimeout(function() {
						$('.list-dates.right-column').removeClass('date7').addClass('date8');
					},6650);

				} else {
					$('.nav-container').removeClass('fixed-nav');
				}
			});
		},
		animateGoDown: function() {
			$('.go-down').click(function(event) {
				$("html, body").animate({ scrollTop: $('#timeline').offset().top - 90 }, 1000, function() {
					$('.go-down').fadeOut();
				});
			});
		},
		toggleFeatures: function(_this) {
			var	moreFeatures = $('#more-features'),
				featuresHeight = $('#more-features').height();

			moreFeatures.toggleClass('open');

			if ( moreFeatures.hasClass('open') ) {
				moreFeatures.addClass('open');
				_this.text('esconder');
			} else {
				moreFeatures.removeClass('open');
				_this.text('ver todas');
			}
		},
		featuresButton: function() {
			$('.view-more-features').click(function(event) {
				event.preventDefault();
				var _this = $(this);
				aboutJS.toggleFeatures(_this);
			});
		}

	};

	aboutJS.loadWaypoints();
	aboutJS.animateGoDown();
	aboutJS.featuresButton();

})(jQuery);