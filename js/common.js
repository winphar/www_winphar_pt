(function ($) {

  var commonJS = {

    loadEssentials: function () {

      $('#header-container').load('../partials/header.html', function () {
          commonJS.updateNavigation();
      });
      $('#footer-container').load('../partials/footer.html');
      $('#modal-container').load('../partials/contact_modal.html', function () {
          commonJS.modalReset();
          setTimeout(function () {
              $('body').css('opacity', 1);
          }, 1000);
      });
    },
    updateNavigation: function () {
        var actualPage = $('body').attr('class');
        $('.navbar-nav li').removeClass('active');
        $('li a.' + actualPage + '').parent('li').addClass('active');
    },
    detectScroll: function () {
        $(window).scroll(function (event) {
            var screenTop = $(document).scrollTop();
            if (screenTop == 0) {
                $('.go-down').fadeIn();
                $('.nav-container').removeClass('fixed-nav');
                $('.cd-top').removeClass('cd-fade-out');
            } else {
                $('.nav-container').addClass('fixed-nav');
            }

            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                $('.cd-top').toggleClass('cd-fade-out');
            }
        });
    },
    backToTop: function () {
        $('.cd-top').click(function (event) {
            event.preventDefault();
            $('body,html').animate({
                scrollTop: 0
            }, 1200);
        });
    },
    placeholderFallback: function () {
        if (!Modernizr.input.placeholder) {
            $('input, textarea').each(function () {
                if ($(this).val() == '' && $(this).attr('placeholder') != '') {
                    $(this).val($(this).attr('placeholder'));
                    $(this).focus(function () {
                        if ($(this).val() == $(this).attr('placeholder')) $(this).val('');
                    });
                    $(this).blur(function () {
                        if ($(this).val() == '') $(this).val($(this).attr('placeholder'));
                    });
                }
            });
        }
    },
    startParalax: function () {
        if (window.matchMedia('(min-width: 780px)').matches) {
            $(window).stellar();
        }
    },
    swipeSupport: function () {
        $("#quotes-carousel, #images-carousel").swiperight(function () {
            $(this).carousel('prev');
        });
        $("#quotes-carousel, #images-carousel").swipeleft(function () {
            $(this).carousel('next');
        });
    },
    modalReset: function () {
        $('#contactModal').bind('hidden.bs.modal', function () {
            $("html").css("margin-right", "0px");
            $("input[required], textarea").css('border-color', '#CFE6F1');
            /*CLEAR FORM STYLES*/
            $('.feedback-text').remove();
        });
        $('#contactModal').bind('show.bs.modal', function () {
            $("html").css("margin-right", "-15px");
            /*CLEAR FORM STYLES*/
            $('.feedback-text').remove();
        });
    },
    sendAjaxForm: function () {
      $(document).on('click', 'button[name="submitted"]', function (event) {
        event.preventDefault();
        var _formButton = $(this);
        commonJS.processAjaxForm(_formButton);
      });
    },
    processAjaxForm: function (_formButton) {
      var proceed = true;
      contactForm = _formButton.parents('form');
      // mark as red each REQUIRED input
      contactForm.find("input[required]").each(function () {
        $(this).css('border-color', '');
        if (!$.trim($(this).val())) {
          //if this field is empty  
          $(this).css('border-color', '#FF7373'); //change border color to #FF7373
          contactForm.find('legend').after('<p class="feedback-text">Preencha o campo assinalado.</p>');
          proceed = false; //set do not proceed flag
        }
        //check invalid email
        var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ($(this).attr("type") == "email" && !email_reg.test($.trim($(this).val()))) {
          $(this).css('border-color', '#FF7373'); //change border color to red
          contactForm.find('.feedback-text').remove();
          contactForm.find('legend').after('<p class="feedback-text">O email não é válido.</p>');
          proceed = false; //set do not proceed flag
        }
      });

      if (!$.trim(contactForm.find('input').val())) { //if this field is empty 
        contactForm.find('.feedback-text').remove();
        contactForm.find('legend').after('<p class="feedback-text">Preencha os campos assinalados.</p>');
      }

      // Clear red style from error input
      contactForm.find("input[required]").focus(function (event) {
        $(this).css('border-color', '#CFE6F1');
        $('.feedback-text').remove();
      });

      if (proceed) {

        var errorMessage = '<p class="feedback-text">Ocorreu um erro durante a submissão do formulário. Por favor, tente mais tarde.</p>';
        var successMessage = '<p class="feedback-text success">Obrigado por nos contactar. Em breve entraremos em contacto consigo.</p>';

        $.ajax({
          url: '../inc/form.php',
          type: 'POST',
          dataType: 'text',
          data: contactForm.serialize(),
          success: function (data, textStatus, jqXHR) {
            contactForm.find('button').blur();
            contactForm.find('button').addClass('loading');
            setTimeout(function () {
              contactForm.find('.feedback-text').remove();
              contactForm.find('button').removeClass('loading');
              contactForm.find('legend').after(successMessage).hide().fadeIn();
            }, 2500);
            setTimeout(function () {
              contactForm.find('p').remove();
            }, 6500);

            //reset values in inputs
            contactForm.find("input[required], textarea").val('');
          },
          error: function (jqXHR, textStatus, errorThrown) {
            contactForm.find('button').blur();
            contactForm.find('button').addClass('loading');
            setTimeout(function () {
              contactForm.find('.feedback-text').remove();
              contactForm.find('button').removeClass('loading');
              contactForm.find('legend').after(errorMessage).hide().fadeIn();
            }, 700);
            setTimeout(function () {
              $('.feedback-text').remove();
            }, 9500);
          }
        });

      } // send form to php

    } // END of processAjaxForm Function

  };

  commonJS.loadEssentials();
  commonJS.detectScroll();
  commonJS.placeholderFallback();
  commonJS.startParalax();
  commonJS.backToTop();
  commonJS.swipeSupport();
  commonJS.sendAjaxForm();

})(jQuery);
