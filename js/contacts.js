(function($) {
	
	var contactsJS = {

		portoMapSettings: function() {

			var styleArray = [{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}];
			var porto_map,
			// Pass the array of styles as well as the name to be displayed on the map type control.
				styledMap = new google.maps.StyledMapType(styleArray, {name: "Winphar"}),
				pinIcon = new google.maps.MarkerImage('../img/marker.svg', null, null, null,new google.maps.Size(35, 50)), 
				//contentString = ''
				//infowindow = new google.maps.InfoWindow(),
				// Create a map object, and include the MapTypeId to add
				// to the map type control.
				porto_MapOptions = {
					zoom: 15,
					icon: pinIcon,
					disableDefaultUI: true,
					draggable: true, //false,
					scrollwheel: false,
					panControl: true,
					center: new google.maps.LatLng(41.1519784,-8.5932673),
					mapTypeControlOptions: {
						mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
					}
				},
				porto_map = new google.maps.Map(document.getElementById('porto-map'), porto_MapOptions),
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(41.1519784,-8.5932673),
					map: porto_map,
					title: 'Winphar',
					icon: pinIcon
				})
				
				lisbon_MapOptions = {
					zoom: 15,
					icon: pinIcon,
					disableDefaultUI: true,
					draggable: true, //false,
					scrollwheel: false,
					panControl: true,
					center: new google.maps.LatLng(38.9036434,-9.0283749),
					mapTypeControlOptions: {
						mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
					}
				},
				lisbon_map = new google.maps.Map(document.getElementById('lisbon-map'), lisbon_MapOptions),
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(38.9036434,-9.0283749),
					map: lisbon_map,
					title: 'Winphar',
					icon: pinIcon
				});

			//google.maps.event.addListener(marker, 'click', function() { infowindow.open(porto_map,marker); });

			//Associate the styled map with the MapTypeId and set it to display.
			porto_map.mapTypes.set('map_style', styledMap);
			porto_map.setMapTypeId('map_style');

			//infowindow.open(porto_map,marker);


		}


	};

	contactsJS.portoMapSettings();

})(jQuery);