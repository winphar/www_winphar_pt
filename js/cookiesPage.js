(function ($) {

  var cookiesJS = {

    animateGoDown: function () {
      $('.go-down').click(function (event) {
        $("html, body").animate({
          scrollTop: $('#cookies-intro').offset().top - 90
        }, 1000, function () {
          $('.go-down').fadeOut();
        });
      });
    },

  };

  cookiesJS.animateGoDown();

})(jQuery);