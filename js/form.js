// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  $(document).on('click', 'button[name="submitted"]', function (event) {
    var _formButton = $(this);
    contactForm = _formButton.parents('form');
    contactForm.validate({
      // Specify validation rules
      rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        fullname: "required",
        email: {
          required: true,
          // Specify that email should be validated
          // by the built-in "email" rule
          email: true
        },
        'privacy[]': {
          required: true,
          minlength: 1
        }
      },
      // Specify validation error messages
      messages: {
        fullname: "<span class='feedback-text'>Preencha o seu nome</span>",
        email: "<span class='feedback-text'>O email não é válido</span>",
        privacy: "<span class='feedback-text'>Ao abrigo do Regulamento Geral de Proteção de Dados, a sua autorização é necessária para submeter o formulário.</span>"
      },
      errorPlacement: function (error, element) {
        if (element.is(":checkbox")) {
          error.appendTo(element.parent().parent());
        } else { // This is the default behavior
          error.insertAfter(element);
        }
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function() {
        event.preventDefault();
        var errorMessage = '<p class="feedback-text">Ocorreu um erro durante a submissão do formulário. Por favor, tente mais tarde.</p>';
        var successMessage = '<p class="feedback-text success">Obrigado por nos contactar. Em breve entraremos em contacto consigo.</p>';
  
        $.ajax({
          url: '../inc/form.php',
          type: 'POST',
          dataType: 'text',
          data: contactForm.serialize(),
          success: function (data, textStatus, jqXHR) {
            contactForm.find('button').blur();
            contactForm.find('button').addClass('loading');
            setTimeout(function () {
              contactForm.find('.feedback-text').remove();
              contactForm.find('button').removeClass('loading');
              contactForm.find('legend').after(successMessage).hide().fadeIn();
            }, 2500);
            setTimeout(function () {
              contactForm.find('p').remove();
            }, 6500);
  
            // reset values in inputs
            contactForm.find("input[required], textarea").val('');
          },
          error: function (jqXHR, textStatus, errorThrown) {
            contactForm.find('button').blur();
            contactForm.find('button').addClass('loading');
            setTimeout(function () {
              contactForm.find('.feedback-text').remove();
              contactForm.find('button').removeClass('loading');
              contactForm.find('legend').after(errorMessage).hide().fadeIn();
            }, 700);
            setTimeout(function () {
              $('.feedback-text').remove();
            }, 9500);
          }
        });
      }
    });
  });

});
