/**
 *
 * Reset scroll position when page reload
 *
*/
window.onload = function() { setTimeout (function () { scrollTo(0,0); }, 0); };
/**
 *
 * Self-Executing Anonymous Function
 *
*/
(function($){

	var homeJS = {

		checkSize: function() {
			if (window.matchMedia('(min-width: 1200px)').matches) {
				var opsMenu_mov = new Waypoint({
					element:  $('.operations.main-section'),
					handler: function(element) {

						var ops_menu = $('.ops_menu'),
							footer = $('footer');

						if ( element == 'down' ) {
							ops_menu.detach();
							ops_menu.insertAfter(footer);

							ops_menu.css({
								'position': 'fixed',
								'top': '155px',
								'margin': 0,
								'z-index': '1000',
								'left': '50%',
								'margin-left': '-35.6rem'
							});

						} else {
							ops_menu.detach();
							ops_menu.insertBefore($('#art-1'));

							ops_menu.css({
								'position': 'absolute',
								'top': '30px'
							});

						}

					}
				})

				var opsMenu_stop = new Waypoint({
					element:  $('#art-3 .ops-feedback'),
					handler: function(element) {

						var screenTop = $('.ops_menu').offset().top;
						var ops_menu = $('.ops_menu');

						if ( element == 'down' ) {

							ops_menu.css({
								'position': 'absolute',
								'top': screenTop
							});
						} else {
							ops_menu.css({
								'position': 'fixed',
								'top': '170px',
								'margin': 0,
								'z-index': '1000',
								'left': '50%',
								'margin-left': '-35.6rem'
							});
						}

					}
				})
			} else if (window.matchMedia('(min-width: 992px)').matches) {
				var opsMenu_mov = new Waypoint({
					element:  $('.operations.main-section'),
					handler: function(element) {

						var ops_menu = $('.ops_menu'),
							footer = $('footer');

						if ( element == 'down' ) {
							ops_menu.detach();
							ops_menu.insertAfter(footer);

							ops_menu.css({
								'position': 'fixed',
								'top': '160px',
								'margin': 0,
								'z-index': '1000',
								'left': '50%',
								'margin-left': '-29.5rem'
							});

						} else {
							ops_menu.detach();
							ops_menu.insertBefore($('#art-1'));

							ops_menu.css({
								'position': 'absolute',
								'top': '30px'
							});

						}

					}
				})

				var opsMenu_stop = new Waypoint({
					element:  $('#art-3 .ops-feedback'),
					handler: function(element) {

						var screenTop = $('.ops_menu').offset().top;
						var ops_menu = $('.ops_menu');

						if ( element == 'down' ) {

							ops_menu.css({
								'position': 'absolute',
								'top': screenTop
							});
						} else {
							ops_menu.css({
								'position': 'fixed',
								'top': '160px',
								'margin': 0,
								'z-index': '1000',
								'left': '50%',
								'margin-left': '-29.5rem'
							});
						}

					}
				})
			} else if (window.matchMedia('(min-width: 768px)').matches) {
				var opsMenu_mov = new Waypoint({
					element:  $('.operations.main-section'),
					handler: function(element) {

						var ops_menu = $('.ops_menu'),
							footer = $('footer');

						if ( element == 'down' ) {
							ops_menu.detach();
							ops_menu.insertAfter(footer);

							ops_menu.css({
								'position': 'fixed',
								'top': '160px',
								'margin': 0,
								'z-index': '1000',
								'left': '50%',
								'margin-left': '-21.9rem'
							});

						} else {
							ops_menu.detach();
							ops_menu.insertBefore($('#art-1'));

							ops_menu.css({
								'position': 'absolute',
								'top': '30px'
							});

						}

					}
				})

				var opsMenu_stop = new Waypoint({
					element:  $('#art-3 .ops-feedback'),
					handler: function(element) {

						var screenTop = $('.ops_menu').offset().top;
						var ops_menu = $('.ops_menu');

						if ( element == 'down' ) {

							ops_menu.css({
								'position': 'absolute',
								'top': screenTop
							});
						} else {
							ops_menu.css({
								'position': 'fixed',
								'top': '160px',
								'margin': 0,
								'z-index': '1000',
								'left': '50%',
								'margin-left': '-21.9rem'
							});
						}

					}
				})
			}

		},
		resizeWindowTimer: function(resizeTimer) {
	        clearTimeout(resizeTimer);
	        resizeTimer = setTimeout(function() {
	            homeJS.checkSize();
	    	}, 100);
		},
		animateGoDown: function() {
			$('.go-down').click(function(event) {
				$("html, body").animate({ scrollTop: $('#manage').offset().top }, 1000, function() {
					$('.go-down').fadeOut();
				});
			});
		},
		randomQuote: function() {
			var textArray = ['<h2><span class="quotes-left"></span><strong>"Com a Sugestão Inteligente de Medicamentos deixei de me preocupar com os objectivos de venda, temos sempre uma noção real do que falta atingir."</strong></h2><blockquote class="quote-author"><p><strong>Dra. Catarina Pires</strong>, Farmácia Vale Milhaços</p></blockquote>','<h2><span class="quotes-left"></span><strong>"É uma ajuda na gestão do tempo e a manter o stock ajustado à procura."</strong></h2><blockquote class="quote-author"><p><strong>Dra. Ana Valério</strong>, Farmácia Mourão Vaz</p></blockquote>','<h2><span class="quotes-left"></span><strong>"A qualquer hora do dia ou da noite, tenho o apoio que preciso da parte da assistência técnica."</strong></h2><blockquote class="quote-author"><p><strong>Dr. Vasco Fonseca</strong>, Farmácia Teixeira Lopes</p></blockquote>', '<h2><span class="quotes-left"></span><strong>"Todo o processo de instalação ocorreu de forma rápida, sem causar nenhum problema ao dia-a-dia da farmácia."</strong></h2><blockquote class="quote-author"><p><strong>Dr. Tiago Gameiro</strong>, Farmácia Campo Grande</p></blockquote>'];
			var randomNumber = Math.floor(Math.random()*textArray.length);

			$('#quotes-carousel .item').html(textArray[randomNumber]);
		},
		loadManageWaypoint: function() {
			var manageBlocks = new Waypoint({
				element: $('.intro-body .trp-btn'),
				handler: function() {
					setTimeout(function() {
						$('.manage-blocks div:nth-child(1)').css('opacity', '1');
					});
					setTimeout(function() {
						$('.manage-blocks div:nth-child(2)').css('opacity', '1');
					},300);
					setTimeout(function() {
						$('.manage-blocks div:nth-child(3)').css('opacity', '1');
					},600);
					$('.nav-container').find('.btn').toggleClass('not-visible');
				}
			})
		},
		loadDifferentWaypoints: function() {
			// Hide OPS MENU after scrolling th gallery
			// So that when you enter the page beyonde the menu area it doesnt appear
			$('#menu-toggler').waypoint(function(direction) { $('.ops_menu').hide(); });
			// Hide btn "Fale connosco"
			// Show again OPS MENU after page down scroll
			$('article#art-4').waypoint(function(direction) { $('.ops_menu').show(); });
			// NAV Menu waypoints
			$('article#art-1').waypoint(function(direction) {
				$('.ops_menu ul li').removeClass('active-item');
				$('.ops_menu ul li[data-id="#art-1"]').addClass('active-item');
			});
			// ARTICLES fade in and out waypoints
			$('article#art-1').waypoint(function(direction) { $('article#art-2').toggleClass('art-visible'); });
			$('article#art-2').waypoint(function(direction) { $('article#art-3').toggleClass('art-visible'); });
			$('article#art-3').waypoint(function(direction) { $('article#art-4').toggleClass('art-visible'); });
			// NAV Menu waypoints
			$('article#art-1 p').waypoint(function(direction) {
				$('.ops_menu ul li').removeClass('active-item');
				$('.ops_menu ul li[data-id="#art-2"]').addClass('active-item');
			}, {offset: '15%' });
			$('article#art-2 p').waypoint(function(direction) {
				$('.ops_menu ul li').removeClass('active-item');
				$('.ops_menu ul li[data-id="#art-3"]').addClass('active-item');
			}, {offset: '15%' });
			$('article#art-3 p').waypoint(function(direction) {
				$('.ops_menu ul li').removeClass('active-item');
				$('.ops_menu ul li[data-id="#art-4"]').addClass('active-item');
			}, {offset: '15%' });
		},
		startLocalScroll: function() {
			$('.ops_menu ul li').localScroll({ 
				hash: true,
				offset: -220,
				onAfter: function(element) {
					$('.ops_menu ul li').removeClass('active-item');
					$('.ops_menu ul li[data-id="'+window.location.hash+'"]').addClass('active-item');
				}
			});
		}

	};

	homeJS.animateGoDown();
	homeJS.loadManageWaypoint();
	homeJS.loadDifferentWaypoints();
	homeJS.startLocalScroll();
	homeJS.randomQuote();
	homeJS.checkSize();
    //Timer for window on resize event
    var resizeTimer;
    window.onresize = function() { homeJS.resizeWindowTimer(resizeTimer); };

})(jQuery);