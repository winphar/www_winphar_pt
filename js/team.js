(function($) {
	
	var teamJS = {

		lazyLiadImages: function() {
			$('#team-panel ul li img').unveil(0, function() {
				$(this).load(function() {
					this.style.opacity = 1;
				});
			});
		},
		animateGoDown: function() {
			$('.go-down').click(function(event) {
				$("html, body").animate({ scrollTop: $('#team-intro').offset().top - 90 }, 1000, function() {
					$('.go-down').fadeOut();
				});
			});
		},


	};

	teamJS.lazyLiadImages();
	teamJS.animateGoDown();

})(jQuery);